import React, { Component } from "react";
import TextTruncate from 'react-text-truncate'
import { Card } from 'antd';

const { Meta } = Card;

function ItemMovie(props) {
    const item = props.item
  return (
    <div>
      <Card
        hoverable
        style={{ width: 240 }}
        cover={
          <img
            alt="example"
            src={props.item.image_url}/>}
            onClick={
                () => {
                    props.onItemMovieClick(item);
                }
            }
      >
        <Meta 
        title={props.item.title} 
        description = {
            <TextTruncate
            line={1}
            truncateText= "..."
            text={props.item.overview}
            TextTruncateChild = {
                <a href = "#"> Read more </a>
            }/>
        }/>
      </Card>
    </div>
  );
}

export default ItemMovie;
