import React from 'react'
import { List } from 'antd';
import ItemMovie from './ItemMovie';

function ListMovie(props){
    console.log('camts: ',props.camts)
    return(
        <div>
        <List
        grid = {{ gutter:16, column:4}}
        dataSource = {props.items}
        renderItem = {item => (
            <List.Item>
                <ItemMovie item= {item} onItemMovieCilck = {props.onItemMovieCilck}></ItemMovie>
                </List.Item>
        )} />
        </div>
    )
}

// function onItemMovieCilck(item){
//     console.log(`click item : ${item}`)
// }

export default ListMovie