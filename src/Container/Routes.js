import React from 'react';
import { Route } from 'react-router-dom';
import LoginPage from './Login'
import MainPage from './Main'
import Login from './Login';

function Routes(){
    return(
        <div>
        <Route exact path="/" component = {Login} />
        <Route exact path="/movie" component = {MainPage} />
        </div>
    )
    
}
export default Routes