import React, { Component } from "react";
import Item from "antd/lib/list/Item";
import { Spin, Modal } from "antd";
import ListMovie from "../Component/ListMovie";
import { Button } from "antd/lib/radio";

class Main extends Component {
  state = {
    items: [],
    isShowModal: false,
    itemMovie: null
  };

  componentDidMount() {
    fetch("https://workshopup.herokuapp.com/movie")
      .then(response => response.json())
      .then(movie => this.setState({ items: movie.results }));
  }

  onItemMovieClick = (item) => {
    console.log(`click item: ${item.title}`);
    this.setState({ isShowModal: true, itemMovie: item });
  };

  handleOk = () => {
      this.setState({ isShowModal: false});
  }

  handleCancel = () => {
    this.setState({ isShowModal: false});
}

  render() {
      const item = this.state.itemMovie
    // console.log("items: ", this.state.items);
    return (
      <div>
        {this.state.items.length > 0 ? (
          <ListMovie items={this.state.items} onItemMovieClick={this.onItemMovieClick} />) : (
          <Spin size="large" tip="Loading..." />
        )}
        {item !== null ?
        <Modal
          width = "40%"
          style = {{maxHeight: '70%'}}
          title= "Basic"
          visible={this.state.isShowModal}
        //   onOk={this.handleOk}
          onCancel={this.onModalClickCancel}
          footer={[
              <Button
              key = "submit"
              type= "primary"
              icon = "heart"
              size = "large"
              shape = "circle"
              onClick= {this.onClickFavorite}>
              </Button>,
              <Button
              key = "submit"
              type= "primary"
              icon = "shopping-cart"
              size = "large"
              shape = "circle"
              onClick= {this.onClickBuyTicket}>
              </Button>
          ]}
        >
          <img src = {item.image_url} style= {{ width: '100%' }}/>
          <p>Some contents...</p>
          <p>Some contents...</p>
          <p>Some contents...</p>
        </Modal>
        :null}
      </div>
    );
  }
}

export default Main;
